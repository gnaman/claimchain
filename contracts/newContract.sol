pragma solidity ^0.4.8;

contract newContract{
    
    // to make event or not to make event 
    // advantage of using event , no need to making calls again and again
    // 
    
    event InsuranceClaimAdded(
        bytes32 contractHash, 
        address address_customer,
        address address_nominee1,
        address address_nominee2,
        address address_nominee3
        );
        
    event MedicalReportAdded(
        bytes32 reportHash,
        address address_customer,
        address address_hospital,
        address address_doctor,
        bytes32 timestamp,
        bytes32 cause_of_death,
        bytes32 medical_history_hash
        );
    event BurialReportAdded (
        bytes32 hash, address customer, address address_funeralDirector
        
        );
    struct InsuranceClaim {
        bytes32 contractHash;
        address address_customer;
        // pending=0, approved=1,rejected =2,formed_notInvoked=3
        uint8 status;
        address address_nominee1;
        address address_nominee2;
        address address_nominee3;
        bytes32 funeralHomeSelected;

    }
    struct DeathCertificate{
        bytes32 certificateHash;
        bytes32 timestamp;
        address issuingOfficerAddress;
        address customer;
    }
    mapping (address=>DeathCertificate) public address_deathCertificate_mapping ;
    function addDeathCertificate(bytes32 certificateHash,
        bytes32 timestamp,
        address issuingOfficerAddress,
        address customer){
            address_deathCertificate_mapping[customer]=DeathCertificate(certificateHash,
         timestamp,
        msg.sender,
         customer);
            
        }
    mapping (address => InsuranceClaim)  public address_claim_mapping; 
    struct MedicalReport {
        bytes32 reportHash;
        address address_customer;
        address address_hospital;
        address address_doctor;
        bytes32 timestamp;
        bytes32 cause_of_death;
        bytes32 medical_history_hash;
        // shared =0, not shared=1
        uint8 status;
    }
    // we will store all the address in the front end for now 
    mapping (address => MedicalReport) public address_medical_report_mapping;
    
    struct BurialReport{
        bytes32 burialReportHash;
        address address_customer;
        address address_funeralDirector;
    }
    mapping (address =>BurialReport ) public address_burial_report_mapping;
    
    function addInsurance (
        bytes32 contractHash, 
        address address_customer,
        address address_nominee1,
        address address_nominee2,
        address address_nominee3
        )
    {
        address_claim_mapping[address_customer]=InsuranceClaim(contractHash
        ,address_customer
        ,3
        ,address_nominee1
        ,address_nominee2
        ,address_nominee3
        ,"null"
        );
        
        InsuranceClaimAdded(contractHash
        ,address_customer
        ,address_nominee1
        ,address_nominee2
        ,address_nominee3
        );
        
    }
     
    function addMedicalReport(
        bytes32 reportHash,
        address address_customer,
        address address_hospital,
        address address_doctor,
        bytes32 timestamp,
        bytes32 cause_of_death,
        bytes32 medical_history_hash
        )
    {
        address_medical_report_mapping[address_customer]=MedicalReport(
            reportHash,
            address_customer,
            address_hospital,
            address_doctor,
            timestamp,
            cause_of_death,
            medical_history_hash,
            1
            );
        MedicalReportAdded( reportHash,
         address_customer,
         address_hospital,
         address_doctor,
         timestamp,
         cause_of_death,
         medical_history_hash);
        
    }
    
    function addBurialReport(bytes32 hash, address customer, address address_funeralDirector)
    {
        address_burial_report_mapping[customer]=BurialReport(
            hash,
            customer,
            address_funeralDirector
            );
        BurialReportAdded(hash,
            customer,
            address_funeralDirector);
    }
    
    
    
}