//import Web3 from 'web3';
var web3;
if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            // set the provider you want from Web3.providers
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
        }

console.log("web3 initilised successfully with account "+web3.eth.accounts[0]);
/*var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));*/
var newContract = web3.eth.contract([
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "address_deathCertificate_mapping",
		"outputs": [
			{
				"name": "certificateHash",
				"type": "bytes32"
			},
			{
				"name": "timestamp",
				"type": "bytes32"
			},
			{
				"name": "issuingOfficerAddress",
				"type": "address"
			},
			{
				"name": "customer",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "hash",
				"type": "bytes32"
			},
			{
				"name": "customer",
				"type": "address"
			},
			{
				"name": "address_funeralDirector",
				"type": "address"
			}
		],
		"name": "addBurialReport",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "address_medical_report_mapping",
		"outputs": [
			{
				"name": "reportHash",
				"type": "bytes32"
			},
			{
				"name": "address_customer",
				"type": "address"
			},
			{
				"name": "address_hospital",
				"type": "address"
			},
			{
				"name": "address_doctor",
				"type": "address"
			},
			{
				"name": "timestamp",
				"type": "bytes32"
			},
			{
				"name": "cause_of_death",
				"type": "bytes32"
			},
			{
				"name": "medical_history_hash",
				"type": "bytes32"
			},
			{
				"name": "status",
				"type": "uint8"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "address_claim_mapping",
		"outputs": [
			{
				"name": "contractHash",
				"type": "bytes32"
			},
			{
				"name": "address_customer",
				"type": "address"
			},
			{
				"name": "status",
				"type": "uint8"
			},
			{
				"name": "address_nominee1",
				"type": "address"
			},
			{
				"name": "address_nominee2",
				"type": "address"
			},
			{
				"name": "address_nominee3",
				"type": "address"
			},
			{
				"name": "funeralHomeSelected",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "certificateHash",
				"type": "bytes32"
			},
			{
				"name": "timestamp",
				"type": "bytes32"
			},
			{
				"name": "issuingOfficerAddress",
				"type": "address"
			},
			{
				"name": "customer",
				"type": "address"
			}
		],
		"name": "addDeathCertificate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "reportHash",
				"type": "bytes32"
			},
			{
				"name": "address_customer",
				"type": "address"
			},
			{
				"name": "address_hospital",
				"type": "address"
			},
			{
				"name": "address_doctor",
				"type": "address"
			},
			{
				"name": "timestamp",
				"type": "bytes32"
			},
			{
				"name": "cause_of_death",
				"type": "bytes32"
			},
			{
				"name": "medical_history_hash",
				"type": "bytes32"
			}
		],
		"name": "addMedicalReport",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"name": "address_burial_report_mapping",
		"outputs": [
			{
				"name": "burialReportHash",
				"type": "bytes32"
			},
			{
				"name": "address_customer",
				"type": "address"
			},
			{
				"name": "address_funeralDirector",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "contractHash",
				"type": "bytes32"
			},
			{
				"name": "address_customer",
				"type": "address"
			},
			{
				"name": "address_nominee1",
				"type": "address"
			},
			{
				"name": "address_nominee2",
				"type": "address"
			},
			{
				"name": "address_nominee3",
				"type": "address"
			}
		],
		"name": "addInsurance",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "contractHash",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "address_customer",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_nominee1",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_nominee2",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_nominee3",
				"type": "address"
			}
		],
		"name": "InsuranceClaimAdded",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "reportHash",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "address_customer",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_hospital",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_doctor",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "timestamp",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "cause_of_death",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "medical_history_hash",
				"type": "bytes32"
			}
		],
		"name": "MedicalReportAdded",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "hash",
				"type": "bytes32"
			},
			{
				"indexed": false,
				"name": "customer",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "address_funeralDirector",
				"type": "address"
			}
		],
		"name": "BurialReportAdded",
		"type": "event"
	}
]);
var contract = newContract.at('0x345ca3e014aaf5dca488057592ee47305d9b3e10');
console.log('the contract is '+JSON.stringify(contract));

function getaccount(){
	        return web3.eth.accounts[0];
}
function getClaimFromAddress()
{
	var address= document.getElementById('nameID').value;
	console.log("inside getClaimFromAddress with address as "+address+typeof address);
	contract.address_claim_mapping(address,function(err,result){
		if(err) throw err;
		console.log(result);
		result1 = result[0].toString();
		// console.log("before "+result1);
		var result1 = hex2a(result1);
		// console.log("after "+result1);
	});
}

$("#getclaimbutton").click(function() {
            //Coursetro.setInstructor($("#name").val(), $("#age").val());
            getClaimFromAddress();
});

function getMedicalRecordFromAddress(address,callback)
{
	// var address= document.getElementById('nameID1').value;
	// var address = 0;
	console.log("inside getMedicalRecordFromAddress with address as "+address+typeof address);
	contract.address_medical_report_mapping(address,function(err,result){
		if(err) throw err;
		console.log('from index.js'+address+result);
		// var result1 = ["1" , "2" , "3" , "4" ,"5" ,"6" , "7","8"];
		// console.log(result1);

		callback(result);
		// if u are getting another character or unwanted character in yr string then take a look at your line number 387 to 390

	});
}

$("#getMedicalRecordButton").click(function() {
            //Coursetro.setInstructor($("#name").val(), $("#age").val());
            console.log("yeah button working");
            getMedicalRecordFromAddress();
});


function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));

    str = str.replace(/[^a-zA-Z0-9 ]/g, "")
    return str;
}

function getBurialRecordFromAddres(callback, address)
{
	var address= address || "0x627306090abab3a6e1400e9345bc60c78a8bef57";
	console.log("inside getBurialRecordFromAddress with address as "+address+typeof address);
	contract.address_burial_report_mapping(address.toString(),function(err,result){
		if(err) throw err;

		console.log(JSON.stringify(result));

		callback(result)
	});
}

function getDeathCertificateFromAddress()
{
	var address= document.getElementById('nameID').value;
	console.log("inside address_deathCertificate_mapping with address as "+address+typeof address);
	contract.address_deathCertificate_mapping(address,function(err,result){
		if(err) throw err;
		console.log(result);
	});
}




